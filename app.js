const express = require('express'),
    app = express(),
    bodyParser = require('body-parser');

// const db = require('./db/');

//temp data until service is connected
const statesData = [
    {Order: 1 , state: "Alabama", abbreviation: "AL", date: "December 14, 1819"},
    {Order: 2 , state: "Alaska", abbreviation: "AK", date: "January 3, 1959"},
    {Order: 3 , state: "Arizona", abbreviation: "AZ", date: "February 14, 1912"},
    {Order: 4 , state: "Arkansas", abbreviation: "AR", date: "June 15, 1836"},
    {Order: 5 , state: "California", abbreviation: "CA", date: "September 9, 1850"}
];
const linesOfBusiness = [
    {lineOfBusiness: "Agrictulture", alsoKnownAs: "AG"},
    {lineOfBusiness: "Commercial", alsoKnownAs: "CM"},
    {lineOfBusiness: "Consumer", alsoKnownAs: "CO"}
];
const usury = [
    {state:"1", line_of_business:"4", consumer_threshold:"50000"},
    {state:"2", line_of_business:"4", consumer_threshold:"75000"},
    {state:"3", line_of_business:"4", consumer_threshold:"99999"},
    {state:"4", line_of_business:"4", consumer_threshold:"400000"}
];

app.use(bodyParser.urlencoded({extended: true}));
app.set('view engine', 'ejs');
app.use(express.static(__dirname + "/public"));

//=============================
// USURY ROUTES
//=============================
app.get('/', function(req, res){
   res.render('./usury/index', {states: statesData, linesOfBusiness: linesOfBusiness});
});

app.get('/usury', function(req, res){
    res.render('./usury/show');
});

app.get('/usury/new', function(req, res){
   res.render('./usury/new');
});


app.listen(3000, function(){
    console.log('End User Admin Server Running');
});

